import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { Geolocation } from 'ionic-native';

import { HomePage } from '../pages/home/home';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = HomePage;

  constructor(platform: Platform) {
    platform.ready().then(() => {
      Geolocation.getCurrentPosition().then((resp) => {
        console.log(resp.coords.latitude + ", " + resp.coords.longitude);
      });
    });
  }
}
