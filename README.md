# Learning Ionic 2 - Using cordova plugins with ionic native #

Retreiving coordinates from ionic-native Geolocation and printing them in console

### Used in this tutorial: 
* Geolocation ionic-native plugin
* Platform ready

Tutorial link:  
http://www.joshmorony.com/using-cordova-plugins-in-ionic-2-with-ionic-native/